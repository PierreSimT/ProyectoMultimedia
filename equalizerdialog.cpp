#include "equalizerdialog.h"
#include "ui_equalizerdialog.h"

#include <VLCQtCore/Audio.h>
#include <VLCQtCore/Equalizer.h>
#include <VLCQtCore/MediaPlayer.h>

EqualizerDialog::EqualizerDialog(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::EqualizerDialog),
      _mediaPlayer(0)
{
    ui->setupUi(this);

    // Default equalizer has ten bands
    _mapSliders.insert(ui->firstBand, 0);
    _mapSliders.insert(ui->secondBand, 1);
    _mapSliders.insert(ui->thirdBand, 2);
    _mapSliders.insert(ui->fourthBand, 3);
    _mapSliders.insert(ui->fifthBand, 4);
    _mapSliders.insert(ui->sixthBand, 5);
    _mapSliders.insert(ui->seventhBand, 6);
    _mapSliders.insert(ui->eighthBand, 7);
    _mapSliders.insert(ui->ninethBand, 8);
    _mapSliders.insert(ui->tenthBand, 9);

    // Set sliders value with unit
    for (QSlider *slider : findChildren<QSlider*>()) {
        QLabel *valueLabel = findChild<QLabel*>(slider->objectName() + "Label");
        connect(slider, &QSlider::valueChanged, this, [=](int value) {
            valueLabel->setText(QString::number(value) + " dB");
        });
    }

    connect(ui->toggleEqualizer, &QCheckBox::toggled, this, &EqualizerDialog::toggle);
}

void EqualizerDialog::setMediaPlayer(VlcMediaPlayer *mediaPlayer)
{
    _mediaPlayer = mediaPlayer;
    auto equalizer = _mediaPlayer->equalizer();
    for (uint i = 0; i < equalizer->presetCount(); i++) {
        ui->presetComboBox->addItem(equalizer->presetNameAt(i));
    }

    // Create local connections
    connect(ui->presetComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), _mediaPlayer->equalizer(), &VlcEqualizer::loadFromPreset);
    connect(_mediaPlayer->equalizer(), &VlcEqualizer::presetLoaded, this, &EqualizerDialog::applySelectedPreset);

    connect(ui->preamp, &QSlider::valueChanged, equalizer, &VlcEqualizer::setPreamplification);
    for (QSlider *slider : findChildren<QSlider*>()) {
        if (slider != ui->preamp) {
            connect(slider, &QSlider::valueChanged, this, &EqualizerDialog::applyChangesForBand);
        }
    }
}

void EqualizerDialog::applyChangesForBand(int value)
{
    int bandIndex = _mapSliders.value(static_cast<QSlider*>(sender()));
    _mediaPlayer->equalizer()->setAmplificationForBandAt((float)value, bandIndex);
}

void EqualizerDialog::applySelectedPreset()
{
    auto equalizer = _mediaPlayer->equalizer();

    disconnect(ui->preamp, 0, equalizer, 0);
    for (QSlider *slider : findChildren<QSlider*>()) {
        if (slider == ui->preamp) {
            slider->setValue(equalizer->preamplification());
        } else {
            disconnect(slider, &QSlider::valueChanged, this, &EqualizerDialog::applyChangesForBand);
            slider->setValue(equalizer->amplificationForBandAt(_mapSliders.value(slider)));
            connect(slider, &QSlider::valueChanged, this, &EqualizerDialog::applyChangesForBand);
        }
    }
    connect(ui->preamp, &QSlider::valueChanged, equalizer, &VlcEqualizer::setPreamplification);
}

void EqualizerDialog::toggle(bool checked)
{
    for (QSlider *slider : findChildren<QSlider*>()) {
        slider->setEnabled(checked);
    }
    ui->presetComboBox->setEnabled(checked);
    _mediaPlayer->equalizer()->setEnabled(checked);
}
