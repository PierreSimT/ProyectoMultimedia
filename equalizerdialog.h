#ifndef EQUALIZERDIALOG_H
#define EQUALIZERDIALOG_H

#include <QDialog>
#include <QSlider>
#include <QMap>

class VlcMediaPlayer;

namespace Ui {
class EqualizerDialog;
}

class EqualizerDialog : public QDialog
{
    Q_OBJECT
public:
    explicit EqualizerDialog(QWidget *parent = 0);

    void setMediaPlayer(VlcMediaPlayer *mediaPlayer);

public slots:
    void applyChangesForBand(int value);

    void applySelectedPreset();

    void toggle(bool checked);

private:
    Ui::EqualizerDialog *ui;

    QMap<QSlider*, int> _mapSliders;
    VlcMediaPlayer *_mediaPlayer;
};

#endif // EQUALIZERDIALOG_H
