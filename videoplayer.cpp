#include "videoplayer.h"
#include "ui_videoplayer.h"

#include <iostream>
#include <QFileDialog>
#include <QInputDialog>
#include <QSysInfo>

#include "equalizerdialog.h"

#include <VLCQtCore/Common.h>
#include <VLCQtCore/Video.h>
#include <VLCQtCore/Instance.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>

VideoPlayer::VideoPlayer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VideoPlayer),
    _media(0),
    _equalizerDialog(new EqualizerDialog(this))
{
    ui->setupUi(this);

    _errorDialog = new QErrorMessage();
    _instance = new VlcInstance(VlcCommon::args(), this);
    _player = new VlcMediaPlayer(_instance);
    _player->setVideoWidget(ui->video);
    _equalizerDialog->setMediaPlayer(_player);

    ui->video->setMediaPlayer(_player);

    connect(ui->actionAbrir, &QAction::triggered, this, &VideoPlayer::abrirArchivo);

    if ( QSysInfo::productType() == "windows")
        connect(ui->actionAbrir_Archivo_de_Prueba, &QAction::triggered, this, &VideoPlayer::abrirPrueba);
    else
        connect(ui->actionAbrir_Archivo_de_Prueba, &QAction::triggered, this, &VideoPlayer::showMsg);

    connect(ui->actionReproducir, &QAction::toggled, _player, &VlcMediaPlayer::togglePause);
    connect(ui->play, &QPushButton::toggled, ui->actionReproducir, &QAction::toggle);
    connect(ui->ecualizador, &QPushButton::clicked, _equalizerDialog, &EqualizerDialog::show);

}

void VideoPlayer::abrirArchivo() {
    QString file =
            QFileDialog::getOpenFileName(this, tr("Abrir Archivo"),
                                         QDir::homePath(),
                                         tr("Todos los archivos(*)"));
    if (file.isEmpty())
        return;

    _media = new VlcMedia(file, true, _instance);
    _player->open(_media);
}

void VideoPlayer::abrirPrueba() {
    QString file;
    file = "prueba.mp4";
    _media = new VlcMedia(file, true, _instance);
    _player->open(_media);
}

void VideoPlayer::showMsg() {
    QString test = "Esta funcion no es soportada por su Sistema Operativo";
    _errorDialog->showMessage(test);
}

VideoPlayer::~VideoPlayer()
{
    delete ui;
}
