#include "videoplayer.h"
#include <QApplication>
#include <VLCQtCore/Common.h>
#include <QtCore/QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationName("Proyecto Multimedia");
    QCoreApplication::setAttribute(Qt::AA_X11InitThreads);

    QApplication a(argc, argv);
    VlcCommon::setPluginPath(a.applicationDirPath() + "/plugins");

    VideoPlayer w;
    w.show();

    return a.exec();
}
